<?php

namespace GoNoWare\Analytics;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class AnalyticsServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishFiles();

        Blade::include('analytics::index', 'analytics');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'analytics');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/analytics.php', 'vendor.analytics');

        view()->composer('analytics::index', function ($view) {
            if (!isset($view->id)) {
                $view->with('id', config('vendor.analytics.id'));
            }
            if (!isset($view->enabled)) {
                $view->with('enabled', config('vendor.analytics.enabled'));
            }
            if (!isset($view->debug)) {
                $view->with('debug', config('vendor.analytics.debug'));
            }

            return $view;
        });
    }

    /**
     * Publish files.
     *
     * @return void
     */
    private function publishFiles()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/analytics.php' => config_path('vendor/analytics.php'),
            ], 'config');
            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/analytics'),
            ], 'public');
            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/analytics'),
            ], 'analytics');
        }
    }
}
