<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Analytics Id
     |--------------------------------------------------------------------------
     |
     | This is the tracking ID. To find your tracking ID:
     |   1. Sign in to your Analytics account: https://analytics.google.com/
     |   2. Click Admin
     |   3. Select an account from the menu in the ACCOUNT column.
     |   4. Select a property from the menu in the PROPERTY column.
     |   5. Under PROPERTY, click Tracking Info > Tracking Code. Your tracking
     |      ID is displayed at the top of the page.
     |
     */

    'id' => env('ANALYTICS_ID', ''),

    /*
     |--------------------------------------------------------------------------
     | Analytics Enabled
     |--------------------------------------------------------------------------
     |
     | By default, Analytics runs only in production mode, when a
     | tracking ID is defined and APP_DEBUG=false is defined in your .env or
     | debug is set to false in config/app.php.
     | You can override the value by setting enabled to true or false instead
     | of null.
     |
     */

    'enabled' => env('ANALYTICS_ENABLED', null),

    /*
     |--------------------------------------------------------------------------
     | Analytics Debug
     |--------------------------------------------------------------------------
     |
     | Analytics provides a debug version that logs detailed messages to the
     | JavaScript console as it is running. You can see exactly what data is
     | being tracked without sending it to Google Analytics.
     | Set the value to true to enable debugging.
     |
     */

    'debug' => env('ANALYTICS_DEBUG', false),

];
