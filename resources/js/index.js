const isDefined = element => typeof element !== 'undefined';

const ga = (...args) => {
  (ga.q = ga.q || []).push(args);
};

const sendPageView = () => {
  const element = window.document.querySelector('[data-analytics-id]');
  if (!isDefined(element)) {
    return;
  }
  const id = element.dataset.analyticsId;
  const debug = element.dataset.analyticsDebug || false;

  if (debug) {
      ga('set', 'sendHitTask', null)
  }

  ga.l = +new Date;
  ga('create', id, 'auto');
  ga('send', 'pageview');
};

window.ga = window.ga || ga;

(() => sendPageView())();
