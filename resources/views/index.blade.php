@if (isset($id[0]) && ($enabled ?? !config('app.debug')))
    <script src="https://ssl.google-analytics.com/analytics{{ $debug ? '_debug' : '' }}.js?id={{ $id }}" type="text/javascript" async></script>
    <script src="{{ asset(mix('js/index.js', 'vendor/analytics')) }}" type="text/javascript" data-analytics-id="{{ $id }}" {{ $debug ? 'data-analytics-debug' : '' }}></script>
@endif
