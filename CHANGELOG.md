# Changelog

All notable changes will be documented in this file.

## 1.2.0 - 2024-07-04

- Add support for Laravel 9, 10 and 11

- Drop support for Laravel 5

## 1.1.0 - 2020-01-14

- Add support for Laravel 6

- Drop support for Laravel 5.5 and PHP 7.1.3

## 1.0.1 - 2019-04-18

- Update service provider

## 1.0.0 - 2018-10-16

- Initial stable release
