# Google Analytics for your Laravel application

[![GitLab Repository](https://img.shields.io/badge/GitLab-gonoware/laravel--analytics-blue.svg?logo=gitlab&style=flat-square&longCache=true)](https://gitlab.com/gonoware/laravel-analytics)
[![Laravel Version](https://img.shields.io/badge/Laravel-11-blue.svg?logo=laravel&style=flat-square&longCache=true)]()
[![Latest Stable Version](https://poser.pugx.org/gonoware/laravel-analytics/v/stable?format=flat-square)](https://packagist.org/packages/gonoware/laravel-analytics)
[![StyleCI](https://gitlab.styleci.io/repos/8015589/shield)](https://gitlab.styleci.io/repos/8015589)
[![License](https://poser.pugx.org/gonoware/laravel-analytics/license?format=flat-square)](https://packagist.org/packages/gonoware/laravel-analytics)
[![Total Downloads](https://poser.pugx.org/gonoware/laravel-analytics/downloads?format=flat-square)](https://packagist.org/packages/gonoware/laravel-analytics)

Using this package you can easily add Google Analytics to your website and 
track page views.


## Installation

This package can be installed through Composer.
```bash
composer require gonoware/laravel-analytics
```

Publish the compiled assets to `public/vendor/analytics` with one of 
these commands:
```bash
php artisan vendor:publish --tag=analytics
```
```bash
php artisan vendor:publish --provider="GoNoWare\Analytics\AnalyticsServiceProvider" --tag=public
```
> When updating, use the `--force` switch to overwrite existing assets:
```bash
php artisan vendor:publish --tag=analytics --force
```

Optionally, you can also publish the config file of this package with this 
command to `config/vendor/analytics.php`:
```bash
php artisan vendor:publish --provider="GoNoWare\Analytics\AnalyticsServiceProvider" --tag=config
```


## Usage

When the installation is done set the tracking ID in your `.env`.
```
ANALYTICS_ID='UA-123456789-1'
``` 

Track a page view by adding the following directive to your
Blade template before the `</body>` closing tag.
```php
@analytics
```
By default, Analytics runs only in production mode, when a tracking ID 
is defined and `APP_DEBUG=false` is set in your `.env` or `debug` is set to 
false in `config/app.php`.
You can override the value by setting `ANALYTICS_ENABLED` to `true` or 
`false` instead of `null`.

Sometimes you may need to pass different values to Analytics. For this 
reason, you can pass an array with keys `enabled` and/or `id` to override the 
default values of the config.
```php
@analytics(['enabled' => app()->environment('staging'), 'id' => 'UA-987654321-1'])
```


## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.


## Security

If you discover any security related issues, please email [em@gonoware.com](mailto:em@gonoware.com) 
instead of using the issue tracker.


## Credits

- [Emanuel Mutschlechner](https://gitlab.com/emanuel)
- [Benedikt Tuschter](https://gitlab.com/benedikttuschter)
- [All Contributors](https://gitlab.com/gonoware/laravel-analytics/graphs/master)


## License

[MIT](LICENSE.md)
 
Copyright (c) 2018-present Go NoWare
 
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Fgonoware%2Flaravel-analytics.svg?type=large)](https://app.fossa.io/projects/git%2Bgitlab.com%2Fgonoware%2Flaravel-analytics?ref=badge_large)
