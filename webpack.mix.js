const mix = require('laravel-mix');

mix.setPublicPath('public')
  .setResourceRoot('../')
  .js('resources/js/index.js', 'js')
  .version();

